libtext-microtemplate-perl (0.24-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtext-microtemplate-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 00:00:48 +0100

libtext-microtemplate-perl (0.24-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove TANIGUCHI Takaki from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Add missing build dependency on libmodule-install-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 20:15:31 +0100

libtext-microtemplate-perl (0.24-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 16:34:49 +0100

libtext-microtemplate-perl (0.24-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Lucas Kanashiro ]
  * Add debian/upstream/metadata
  * Import upstream version 0.24
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.6
  * Add autopkgtest-pkg-perl
  * Update upstream copyright

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sat, 15 Aug 2015 04:19:37 -0300

libtext-microtemplate-perl (0.20-1) unstable; urgency=low

  * Imported Upstream version 0.20

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 21 Jun 2013 17:49:18 +0900

libtext-microtemplate-perl (0.19-1) unstable; urgency=low

  [ gregor herrmann ]
  * Remove version from perl (build) dependency.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ TANIGUCHI Takaki ]
  * debian/copyright: Update Format URL
  * Imported Upstream version 0.19
  * debian/control: Bump Standards-Version to 3.9.4 (with no changes).

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 08 May 2013 12:56:45 +0900

libtext-microtemplate-perl (0.18-1) unstable; urgency=low

  * Initial Release. (Closes: #612871)

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 16 Feb 2011 11:02:29 +0900
